import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import ProjectPreview from "../components/project-preview";

const IndexPage = () => {
  const data = useStaticQuery(graphql`
    {
      allProjectsJson {
        edges {
          node {
            title
            slug
            url
            description
            image {
              id
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  `)

  const projects = data.allProjectsJson.edges

  return (
    <Layout>
      <SEO title=" " />
      <h1>Hello World!</h1>
      <p>Welcome to my portfolio site.</p>
      <table>
      <tr>
      
      {projects.map(({ node: project }) => {
        const title = project.title;
        const description = project.description;
        const slug = project.slug;
        const imageData = project.image.childImageSharp.fluid;
        for (let i=0; i <projects.length; i++) {
          let cellData = (<ProjectPreview
          title={title}
          description={description}
          slug={slug}
          imageData={imageData} />)
          return (<td>
                {cellData}
                </td>)
        } 
      }
      )}
      
      </tr>
      </table>

      {/* <p>Now go build something great.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />

    </div> */}
      <Link to="/page-2/">Go to page 2</Link>

    </Layout>
  )
}

export default IndexPage
